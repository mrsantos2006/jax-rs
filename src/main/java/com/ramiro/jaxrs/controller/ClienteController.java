package com.ramiro.jaxrs.controller;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.ramiro.jaxrs.filter.CheckClientRequestFilter;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;


@Stateless
@Path("cliente")
public class ClienteController {
	
	public ClienteController() {
		System.out.println("Entrou construtor ClienteController");
	}

	@GET
	@Path("mensagem")
	@Produces({MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
	public String olaMundoText() {
		return "Olá cliente!";
	}
	
	@GET
	@Path("template")
	@Produces(MediaType.TEXT_HTML)
	public String olaMundoTemplate() {
		return "ola mundooooo";
	}
	
	@GET
	@Path("starwars/{path}/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response chamarAPI(@PathParam("path") String path, @PathParam("id") String id) {
		System.out.println("Entrou ClienteController - starwars");
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(CheckClientRequestFilter.class);
		Client client = ClientBuilder.newClient(clientConfig);
		
		WebTarget target = client.target("https://swapi.dev/api");
		
		jakarta.ws.rs.core.Response response = 
				target
				.path(path)
				.path(id)
				.request()
				.header("Accept", "application/json")
				.get();
		
		int status = response.getStatus();
		MediaType mediaType = response.getMediaType();

		return Response.status(status)
	                .entity(response.readEntity(String.class)).type(mediaType.toString()).build();
		

	}
}
