package com.ramiro.jaxrs;

import javax.enterprise.inject.Produces;
import javax.ws.rs.ext.Provider;

import com.ramiro.binder.ServiceBind;

@Provider
public class ServiceBindProducer {

	@Produces
	public ServiceBind getServiceBind() {
		System.out.println("criou um novo ServiceBind");
		return new ServiceBind();
	}
}
