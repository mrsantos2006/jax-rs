package com.ramiro.jaxrs.filter;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;


@Provider
public class CheckContainerResponseFilter implements ContainerResponseFilter {

	private final String TEXT_PLAIN = "text/plain";
	private final String TEXT_PLAIN_UTF_8 = "text/plain; charset=utf-8";
	private final String CONTENT_TYPE = "Content-Type";
	private final String CHARSET_UTF_8 = "charset=utf-8";

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		responseContext.getHeaders().add("X-Powered-By", "Ramiro :-)");

		MultivaluedMap<String, Object> headers = responseContext.getHeaders();

		this.tratarNotFound(responseContext);
		this.tratarStatusDiferente200(headers, responseContext.getStatus());
		this.tratarHeaderTextPlainInclusaoUTF8(headers, responseContext.getStatus());
		
	}

	private void tratarNotFound(ContainerResponseContext responseContext) {
		
		if(responseContext.getStatus() == 404) {
			responseContext.setEntity("Ops... Nao localizamos o seu dado");
			responseContext.getHeaders().add(this.CONTENT_TYPE, this.TEXT_PLAIN);
		}
		
	}

	private void tratarStatusDiferente200(MultivaluedMap<String, Object> headers, int status) {
		if (status != 200) {
			if (headers.containsKey(this.CONTENT_TYPE)) {
				headers.remove(this.CONTENT_TYPE);
				headers.add(this.CONTENT_TYPE, this.TEXT_PLAIN_UTF_8);
			}
		}
	}

	private void tratarHeaderTextPlainInclusaoUTF8(MultivaluedMap<String, Object> headers, int status) {

		if (headers.containsKey(this.CONTENT_TYPE)) {

			List<String> conteudosTratados = headers.get(this.CONTENT_TYPE).stream()
					.map(obj -> obj.toString().toLowerCase().contains(this.TEXT_PLAIN)
							&& !obj.toString().toLowerCase().contains(this.CHARSET_UTF_8) ? this.TEXT_PLAIN_UTF_8
									: obj.toString())
					.collect(Collectors.toList());

			headers.remove(this.CONTENT_TYPE);
			conteudosTratados.stream().forEach(conteudo -> {
				headers.add(this.CONTENT_TYPE, conteudo);
			});

		}

	}

}
